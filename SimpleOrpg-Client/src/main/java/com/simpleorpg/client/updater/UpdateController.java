package com.simpleorpg.client.updater;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Date;

public class UpdateController {
	private final String website;
	
	public UpdateController(String ip, int port) {
		this.website = "http://" + ip + ":" + String.valueOf(port);
	}
	
	public ReadableByteChannel download(String downloadPath) throws IOException  {
		URL url = new URL(website + "/" + downloadPath);
	    ReadableByteChannel rbc = Channels.newChannel(url.openStream());
	    return rbc;
	}
	
	public void downloadAndSave(String downloadPath) throws IOException {
		ReadableByteChannel rbc = download(downloadPath);
		String savePath = "resources/" + downloadPath;
		new File(new File(savePath).getParent()).mkdirs();
	    FileOutputStream fos = new FileOutputStream(savePath);
	    fos.getChannel().transferFrom(rbc, 0, 1 << 24);
	}

	public long getLastModified(String remotePath) throws IOException {
		URL url = new URL(website + "/" + remotePath);  
		URLConnection urlConnection = url.openConnection();  
		Date lastDate = new Date(urlConnection.getLastModified());  
		return lastDate.getTime();
	}

}
