package com.simpleorpg.server.net;

import java.net.Socket;

public class LoginHandler extends MessageHandler {
	
	private String payload;
	public LoginHandler(String payload) {
		this.payload = payload;
	}

	@Override
	public void handleMessage(Socket socket) {
		synchronized(this) {
			//log(Level.DEBUG, MSG_TYPE.REC, socket, "LOGIN:"+payload);
			
			if (payload.contains(",")) {
				String userName = payload.split(",")[0].toLowerCase();
				String password = payload.substring(userName.length() + 1);
	
				if (validation.validUsername(userName) && validation.validPassword(password)) {
					if (db.correctPassword(userName, password)) {
						if (!users.containsValue(userName)) {
							users.put(socket, userName);
							MessageHandler joinHandler = new JoinGameHandler();
							joinHandler.handleMessage(socket);
						} else {
							MessageHandler errorHandler = new AlertHandler("User is already online");
							errorHandler.handleMessage(socket);
							closeSocket(socket);
						}
					} else {
						MessageHandler errorHandler = new AlertHandler("Incorrect username/password");
						errorHandler.handleMessage(socket);
						closeSocket(socket);
					}
				} else {
					closeSocket(socket);
				}
			}
		}
	}

}
