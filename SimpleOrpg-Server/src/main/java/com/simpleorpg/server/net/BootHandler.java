package com.simpleorpg.server.net;

import java.net.Socket;

import org.apache.log4j.Level;

import com.simpleorpg.server.Player;

public class BootHandler extends MessageHandler {
	private String payload;
	public BootHandler(String payload) {
		this.payload = payload;
	}

	@Override
	public void handleMessage(Socket socket) {
		synchronized(this) {
			log(Level.DEBUG, MSG_TYPE.REC, socket, "BOOT:"+payload);
			Player yourPlayer = players.get(socket);
			if (yourPlayer.getAdmin() > 0) {
				payload = payload.trim();
				if (validation.validPlayer(payload)) {
					for (Socket sock : players.keySet()) {
						Player player = players.get(sock);
						if (player.getName().equals(payload) && player.getAdmin() <= 0) {
							String bootMessage = "CHAT:BROADCAST,#FF7700," + player.getName() + " has been booted by " + yourPlayer.getName() + "!";
							sendAll(bootMessage);
							new AlertHandler("You have been booted!").handleMessage(sock);
							new LeaveGameHandler().handleMessage(sock);
							break;
						}
					}
				}
			} else {
				String errorMessage = "CHAT:BROADCAST,#FF0000,Invalid admin access for BOOT!";
				sendTo(socket,errorMessage);
			}
		}
	}
}
