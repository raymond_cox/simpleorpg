package com.simpleorpg.client;

import org.newdawn.slick.Font;
import org.newdawn.slick.gui.GUIContext;

public class PasswordTextField extends TabbedTextField {
	
	private String value = "";

	public PasswordTextField(GUIContext container, Font font, int x, int y,
			int width, int height, int order) {
		super(container, font, x, y, width, height, order);	
	}
	
	@Override
	public void keyPressed(int key, char c) {
		int oldLength = super.getText().length();
		super.keyPressed(key, c);
		try {
			if (oldLength < super.getText().length()) {
				value += super.getText().substring(super.getText().length()-1);
				super.setText(super.getText().substring(0, super.getText().length()-1) + "*");
			}
		} catch (Exception ex) { }
	}
	
	@Override
	public void setText(String value) {
		super.setText(value);
		this.value = value;
	}
	
	@Override
	public String getText() {
		return value;
	}
}
