package com.simpleorpg.client;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.apache.log4j.Logger;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import com.artemis.Entity;
import com.artemis.EntitySystem;
import com.artemis.SystemManager;
import com.artemis.World;
import com.simpleorpg.client.components.DrawableText;
import com.simpleorpg.client.components.Visibility;
import com.simpleorpg.client.states.ConnectingState;
import com.simpleorpg.client.states.GameState;
import com.simpleorpg.client.states.LoginState;
import com.simpleorpg.client.systems.NetworkingSystem;
import com.simpleorpg.common.ResourceFactory;


public class Client extends StateBasedGame {
	private static final Logger logger = Logger.getLogger(Client.class);
	private World world;
	private SystemManager systemManager;
	private EntitySystem networkingSystem;
	
	public Client() {
		super("Simple Orpg");
	}
	
	public static void main(String[] args) {
		ResourceFactory factory = ResourceFactory.getInstance();
		try {
			factory.loadResources(new FileInputStream("resources/resources.xml"));
		} catch (FileNotFoundException ex) {
			logger.error("Unable to find resources.xml", ex);
		} catch (Exception ex) {
			logger.error(ex);
		}
		new Client().load();
	}
	
	public void load() {
		logger.debug("Initializing game engine");
		try {
			AppGameContainer container = new AppGameContainer(this);
			container.setDisplayMode(800, 600, false);
			//container.setDisplayMode(1024, 768, false);
			container.start();
		} catch (SlickException ex) {
			logger.fatal(ex);
		}
	}

	@Override
	public void initStatesList(GameContainer container) throws SlickException {
		world = new World();
		systemManager = world.getSystemManager();
		networkingSystem = systemManager.setSystem(new NetworkingSystem(this));
		systemManager.initializeAll();
		
		// Initialize your player
		Entity player = world.createEntity();
		player.setGroup("PLAYER");
		player.setTag("YOU");
		
		// Setup the in game chat screen
		Entity input = world.createEntity();
		input.setTag("INPUT");
		input.addComponent(new DrawableText());
		input.addComponent(new Visibility(true));
		
		addState(new LoginState(world));
		addState(new ConnectingState(world));
		addState(new GameState(world));	
	}
	
	@Override
	protected void postUpdateState(GameContainer container, int delta)
			throws SlickException {
		super.postUpdateState(container, delta);
		world.loopStart();
		world.setDelta(delta);
		networkingSystem.process();
	}
}
