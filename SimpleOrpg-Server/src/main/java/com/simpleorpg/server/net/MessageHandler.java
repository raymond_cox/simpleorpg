package com.simpleorpg.server.net;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.simpleorpg.common.Enemy;
import com.simpleorpg.common.NewTiledMap;
import com.simpleorpg.common.ResourceFactory;
import com.simpleorpg.common.ResourceManager;
import com.simpleorpg.common.Validation;
import com.simpleorpg.server.Database;
import com.simpleorpg.server.Map;
import com.simpleorpg.server.Player;


public abstract class MessageHandler {

	private static final Logger messageLogger = Logger.getLogger(MessageHandler.class);
	protected final Logger logger = Logger.getLogger(getClass());
	protected static final HashMap<String, Map> maps = new HashMap<String, Map>();
	protected static final HashMap<Socket, Player> players = new HashMap<Socket, Player>();
	protected static final HashMap<Socket, String> users = new HashMap<Socket, String>();
	protected static final Database db = Database.getInstance();
	protected final Validation validation = Validation.getInstance();
	protected enum MSG_TYPE {
		SND, REC
	}
	
	// Load in maps from the database
	public static void init() {
		for (final String mapRef : ResourceFactory.getInstance().getResourceIds("tiledmap")) {
			messageLogger.info("Loading " + mapRef);
			final Map activeMap = new Map();
			maps.put(mapRef, activeMap);
			ResourceManager manager = ResourceManager.getInstance();
			final NewTiledMap tiledMap = (NewTiledMap)manager.getResource(mapRef, true).getObject();
			
			// Spawn enemies on map
			final ArrayList<Enemy> enemies = new ArrayList<Enemy>();
			int groupID = 0;
			int tw = tiledMap.getTileWidth();
			int th = tiledMap.getTileHeight();
			for (int objectID = 0; objectID < tiledMap.getObjectCount(groupID); objectID++) {
				int objectX = tiledMap.getObjectX(groupID, objectID);
				int objectY = tiledMap.getObjectY(groupID, objectID);
				int objectWidth = tiledMap.getObjectWidth(groupID, objectID);
				int objectHeight = tiledMap.getObjectHeight(groupID, objectID);
				String objectType = tiledMap.getObjectType(groupID, objectID);
				
				if (objectType.equalsIgnoreCase("Spawn")) {
					String enemyId = tiledMap.getObjectProperty(groupID, objectID, "Enemy", "");
					int number = Integer.valueOf(tiledMap.getObjectProperty(groupID, objectID, "Number", ""));
					Enemy enemy = db.selectEnemy(enemyId);
					int num = 0;
					for (int i=0; i<number; i++) {
						enemy = new Enemy(enemy);
						enemy.setId(enemyId + num);
						enemy.setX((int)(objectX + Math.random() * objectWidth)/tw);
						enemy.setY((int)(objectY + Math.random() * objectHeight)/th);
						enemies.add(enemy);
						num = num + 1;
					}
				}
				
			}
			activeMap.setEnemies(enemies);
			
			// Send enemy movement updates to players
			new Thread(new Runnable() {
				public void run() {
					while (true) {						
						synchronized(this) {
							for (Enemy enemy : activeMap.getEnemies()) {
								int newX = enemy.getX() + (int)(Math.random() * 3) - 1;
								int newY = enemy.getY() + (int)(Math.random() * 3) - 1;
								if (newY < tiledMap.getHeight() && newX < tiledMap.getWidth() && 
									newY >= 0 && newX >= 0 && tiledMap.getTileId(newX, newY, 3) == 0) {
									enemy.setX(newX);
									enemy.setY(newY);
									String enemyMoved = "ENEMY_MOVED:" + enemy.getId() + "," + enemy.getX() + "," + enemy.getY();
									sendAllMap(mapRef, enemyMoved);
								}
							}
						}
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
						}
					}
				}
				
			}).start();
			
			
			
		}
		
	}
	
	public static MessageHandler create(String message) {		
		String id = message;
		if (message.contains(":")) {
			id = message.split(":")[0].toUpperCase();
		}
		int idIndex = message.indexOf(":");
		String payload = "";
		if (idIndex+1 < message.length()) {
			payload = message.substring(idIndex+1);
		}
		
		if (id.equals("MOVE")) {
			return new MoveHandler(payload);
		} else if (id.equals("CHAT")) {
			return new ChatHandler(payload);
		} else if (id.equals("WHO")) {
			return new WhoHandler();
		} else if (id.equals("SET_NAME")) {
			return new SetNameHandler(payload);
		} else if (id.equals("LOGIN")) {
			return new LoginHandler(payload);
		} else if (id.equals("NEW")) {
			return new NewAccountHandler(payload);
		} else if (id.equals("BOOT")) {
			return new BootHandler(payload);
		}
		
//		if (id.equals("JOIN_GAME")) {
//			return new JoinGameHandler();
//		} else if (id.equals("LEAVE_GAME")) {
//			return new LeaveGameHandler();
//		} else if (id.equals("JOIN_MAP")) {
//			return new JoinMapHandler();
//		} else {
			messageLogger.warn(id + " ID does not exist");
			return null;
//		}
	}
	
	protected static synchronized void sendAllMap(String mapRef, String message) {
		Map map = maps.get(mapRef);
		
		for (Socket otherSocket : map.getPlayers().keySet()) {
			sendTo(otherSocket, message);
		}
	}
	
	protected static synchronized void sendAllMapBut(Socket socket, String message) {
		Player yourPlayer = players.get(socket);
		Map map = maps.get(yourPlayer.getMapRef());
		
		for (Socket otherSocket : map.getPlayers().keySet()) {
			if (players.get(otherSocket).getId() != yourPlayer.getId()) {
				sendTo(otherSocket, message);
			}
		}
	}
	
	protected static synchronized void sendAll(String message) {
		for (Socket socket : players.keySet()) {
			sendTo(socket, message);
		}
	}
	
	protected static void sendTo(Socket socket, String message) {
		if (socket != null && !socket.isClosed() && !message.isEmpty()) {
			try {
				PrintWriter playerOut = new PrintWriter(socket.getOutputStream(), true);
				playerOut.println(message);
				log(Level.DEBUG, MSG_TYPE.SND, socket, message);
			} catch (Exception ex) {
				log(Level.ERROR, socket, message, ex.getCause());
			}
		}
	}
	
	protected static String getIp(Socket socket) {
		return socket.getInetAddress().getHostAddress();
	}
	
	protected static synchronized void log(Level level, MSG_TYPE type, Socket socket, String msg) {
		String name = "";
				
		if (socket != null &&  !socket.isClosed()) {
			name = socket.getInetAddress().getHostAddress();
			Player yourPlayer = players.get(socket);
			if (yourPlayer != null) {
				name = yourPlayer.getName();
			}
		}
		
		if (!name.equals("")) {
			messageLogger.log(level, type + " [" + name +"] " + msg);
		} else {
			messageLogger.log(level, type + " " + msg);
		}
	}
	
	protected static synchronized void log(Level level, Socket socket, String msg, Throwable throwable) {
		String name = "";
		
		if (socket != null &&  !socket.isClosed()) {
			name = socket.getInetAddress().getHostAddress();
			Player yourPlayer = players.get(socket);
			if (yourPlayer != null) {
				name = yourPlayer.getName();
			}
		}
		
		if (!name.equals("")) {
			messageLogger.log(level, "[" + name +"] " + msg, throwable);
		} else {
			messageLogger.log(level, msg, throwable);
		}
	}
	
	protected static void closeSocket(Socket socket) {
		try {
			if (!socket.isClosed()) socket.close();
		} catch (IOException e) {
			messageLogger.debug(e);
		}
	}
	
	
	
	public abstract void handleMessage(Socket socket);

}
