package com.simpleorpg.server.net;

import java.net.Socket;

import org.apache.log4j.Level;

import com.simpleorpg.server.Player;

public class SetNameHandler extends MessageHandler {
	
	private String payload;
	public SetNameHandler(String payload) {
		this.payload = payload;
	}

	@Override
	public void handleMessage(Socket socket) {
		synchronized(this) {
			log(Level.DEBUG, MSG_TYPE.REC, socket, "SET_NAME:"+payload);
			Player yourPlayer = players.get(socket);
			if (yourPlayer.getAdmin() > 0) {
				payload = payload.trim();
				if (validation.validPlayer(payload)) {
					yourPlayer.setName(payload);
					sendAllMapBut(socket, "SET_NAME:" + yourPlayer.getId() + "," + payload);
					sendTo(socket, "SET_NAME:YOU," + payload);
				}
			} else {
				String errorMessage = "CHAT:BROADCAST,#FF0000,Invalid admin access for SETNAME!";
				sendTo(socket,errorMessage);
			}
		}
	}

}
