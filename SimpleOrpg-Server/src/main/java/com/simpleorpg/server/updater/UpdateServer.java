package com.simpleorpg.server.updater;
import java.util.Properties;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.server.nio.SelectChannelConnector;

import com.simpleorpg.common.PropertiesLoader;

public class UpdateServer implements Runnable {

	public void run() {
        Server server = new Server();
        SelectChannelConnector connector = new SelectChannelConnector();
        Properties props = PropertiesLoader.getInstance().loadProperties("server.properties");
        int updatePort = Integer.valueOf(props.getProperty("update_port", "8080"));
        connector.setPort(updatePort);
        server.addConnector(connector);
 
        ResourceHandler resource_handler = new ResourceHandler();
        resource_handler.setDirectoriesListed(true);
        resource_handler.setWelcomeFiles(new String[]{ "index.html" });
 
        resource_handler.setResourceBase("resources");
 
        HandlerList handlers = new HandlerList();
        handlers.setHandlers(new Handler[] { resource_handler, new DefaultHandler() });
        server.setHandler(handlers);
 
        try {
			server.start();
			server.join();
		} catch (Exception e) {
			e.printStackTrace();
		}
        		
	}
	
}
