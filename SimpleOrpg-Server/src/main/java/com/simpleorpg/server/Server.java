package com.simpleorpg.server;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.simpleorpg.common.PropertiesLoader;
import com.simpleorpg.common.ResourceFactory;
import com.simpleorpg.server.net.LeaveGameHandler;
import com.simpleorpg.server.net.MessageHandler;
import com.simpleorpg.server.updater.UpdateServer;

public class Server {
	private static final Logger logger = Logger.getLogger(Server.class);
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		final ServerSocket serverSocket;
		PropertiesLoader propertiesLoader = PropertiesLoader.getInstance();
		final Properties serverProperties = propertiesLoader.loadProperties("server.properties");
		int port = Integer.valueOf(serverProperties.getProperty("game_port", "1234"));
		
		ResourceFactory factory = ResourceFactory.getInstance();
		try {
			factory.loadResources(new FileInputStream("resources/resources.xml"));
		} catch (FileNotFoundException e) {
			logger.error("Resources file not found", e);
		} catch (Exception e) {
			logger.error(e);
		}
		
		// Start the update server
		new Thread(new UpdateServer()).start();
		
		MessageHandler.init();
		
		try {
			serverSocket = new ServerSocket(port);
			logger.info("Listening on port " + port);
			
			// Accept a new connection
			new Thread() {
				public void run() {
					while (true) {
						try {
							final Socket clientSocket = serverSocket.accept();
							clientSocket.setTcpNoDelay(true);
							logger.info("Accepted " + clientSocket.getInetAddress().getHostAddress());
							
							new Thread() {
								public void run() {
									BufferedReader in = null;
									
									try {
										in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
										String receivedLine;
										while ((receivedLine = in.readLine()) != null) {
											MessageHandler handler = MessageHandler.create(receivedLine);
											if (handler != null) {
												handler.handleMessage(clientSocket);
											}
										}
										in.close();
										
									} catch (Exception ex) {
										logger.error(ex);
									}
									MessageHandler leaveHandler = new LeaveGameHandler();
									leaveHandler.handleMessage(clientSocket);									
								}
							}.start();
						} catch (Exception ex) {
							logger.error(ex);
						}
					}
				}
			}.start();
			
		} catch (Exception ex) {
			logger.fatal(ex);
		}
	}

}
