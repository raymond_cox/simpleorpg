package com.simpleorpg.client.updater;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.HashMap;
import java.util.Properties;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;

import org.apache.log4j.Logger;
import org.w3c.dom.Element;

import com.simpleorpg.client.Client;
import com.simpleorpg.common.PropertiesLoader;
import com.simpleorpg.common.ResourceFactory;

public class UpdateClient extends JFrame {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(UpdateClient.class);
	public JLabel updateStatusLabel = new JLabel("");
	public JProgressBar updateProgress = new JProgressBar();
	
	public static void main(String[] args) {
		UpdateClient updateClient = new UpdateClient("Simple Updater");
		updateClient.setVisible(true);
		Properties props = PropertiesLoader.getInstance().loadProperties("client.properties");
		String ip = props.getProperty("update_ip", "127.0.0.1");
		int port = Integer.valueOf(props.getProperty("update_port", "8080"));
		UpdateController controller = new UpdateController(ip, port);
		ResourceFactory factory = ResourceFactory.getInstance();

		try {
			// Download resources.xml
			updateClient.updateStatusLabel.setText("Downloading resources.xml...");
			ReadableByteChannel rbc = controller.download("resources.xml");
			InputStream in = Channels.newInputStream(rbc);
			updateClient.updateProgress.setValue(1);		
			HashMap<String, Element> resourceElements = null;
			try {
				resourceElements = factory.loadResources(in);
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(updateClient, "Unable to parse resources.xml");
				logger.error("Unable to parse resources.xml", ex);
				System.exit(0);
			}
			updateClient.updateProgress.setMaximum(resourceElements.values().size());
			String errors = "";
			// Check for outdated resources and download them
			for (Element element : resourceElements.values()) {
				String path = element.getAttribute("path");
				updateClient.updateStatusLabel.setText("Checking " + path + "...");
				File localFile = new File("resources/" + path);
				if (!localFile.exists() || localFile.lastModified() < controller.getLastModified(path)) {
					updateClient.updateStatusLabel.setText("Downloading " + path + "...");
					try {
						controller.downloadAndSave(path);
					} catch (FileNotFoundException ex) {
						errors += "\n" + path + " not found";
						logger.error(path + " not found", ex);
					}
				}
				updateClient.updateProgress.setValue(updateClient.updateProgress.getValue()+1);
			}
			
			if (!errors.isEmpty()) {
				JOptionPane.showMessageDialog(updateClient, "There were errors duing the update!\n" + errors, "Error", JOptionPane.ERROR_MESSAGE);
			}
			
			updateClient.setVisible(false);
			
		} catch (IOException ex) {
			JOptionPane.showMessageDialog(updateClient, "Unable to connect to update server!", "Error", JOptionPane.ERROR_MESSAGE);
			logger.error("Unable to connect to update server", ex);
			System.exit(0);
		}
		
		// After updating start the client
		Client client = new Client();
		client.load();

	}
	
	public UpdateClient(String title) {
		this.setTitle(title);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setPreferredSize(new Dimension(400,200));
		this.pack();
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(10,0,10,0);
		c.gridx = 0;
		c.gridy = 0;
		this.add(updateStatusLabel, c);
		c.gridy = 1;
		this.add(updateProgress, c);
	}
}
