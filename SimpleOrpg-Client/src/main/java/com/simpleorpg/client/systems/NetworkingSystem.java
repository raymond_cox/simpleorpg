package com.simpleorpg.client.systems;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.ArrayBlockingQueue;

import org.lwjgl.Sys;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.simpleorpg.client.components.ColorComponent;
import com.simpleorpg.client.components.DrawableText;
import com.simpleorpg.client.components.Location;
import com.simpleorpg.client.components.Networking;
import com.simpleorpg.client.components.ResourceRef;
import com.simpleorpg.client.components.ChatBubble;
import com.simpleorpg.client.components.Timer;
import com.simpleorpg.client.components.Warp;
import com.simpleorpg.client.states.ConnectingState;
import com.simpleorpg.client.states.GameState;
import com.simpleorpg.client.states.LoginState;

public class NetworkingSystem extends BaseEntityProcessingSystem {
	private ComponentMapper<Networking> networkingMapper;
	private ComponentMapper<ResourceRef> resourceRefMapper;
	private ComponentMapper<DrawableText> drawableTextMapper;
	private ComponentMapper<Location> locationMapper;
	private StateBasedGame game;

	@SuppressWarnings("unchecked")
	public NetworkingSystem(StateBasedGame game) {
		super(Networking.class);
		this.game = game;
	}

	@Override
	protected void initialize() {
		networkingMapper = new ComponentMapper<Networking>(Networking.class, world);
		resourceRefMapper = new ComponentMapper<ResourceRef>(ResourceRef.class, world);
		drawableTextMapper = new ComponentMapper<DrawableText>(DrawableText.class, world);
		locationMapper = new ComponentMapper<Location>(Location.class, world);
	}

	@Override
	protected void process(Entity e) {
		final Networking networking = networkingMapper.get(e);		
		if (!networking.isConnected()) {
			try {
				if (!networking.isConnecting() && game.getCurrentStateID() == ConnectingState.ID) {
					networking.setConnecting(true);
					// Connect to the server
					new Thread() {
						public void run() {
							try {
								String ip = networking.getIp();
								int port = networking.getPort();
								logger.info("Connecting to " + ip + ":" + port);
								final Socket clientSocket = new Socket(ip, port);
								clientSocket.setTcpNoDelay(true);
								networking.setSocket(clientSocket);
								
								// Start listening for messages from the server
								BufferedReader in = null;
								PrintWriter out = null;
								try {
									in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
									out = new PrintWriter(clientSocket.getOutputStream(), true);
									networking.setOut(out);
									networking.setConnected(true);
									logger.info("Connected to server");
									game.enterState(GameState.ID);
									String receivedLine;
									while ((receivedLine = in.readLine()) != null) {
										logger.info(receivedLine);
										networking.getReceivedMessages().put(receivedLine);
									}
									
								} catch (Exception ex) {
									logger.fatal(ex);
									game.enterState(LoginState.ID);
								}
							} catch (Exception ex) {
								logger.fatal(ex);
							}
							logger.info("Disconnected from server");
							networking.setConnected(false);
							networking.setConnecting(false);
						}
					}.start();
				}
				
			} catch(Exception ex) {
				logger.fatal(ex);
			}
			
		// Handle messages
		} else {
			ArrayBlockingQueue<String> sendQueue = networking.getSendMessages();
			sendMessages(sendQueue, networking.getOut());
		}
		ArrayBlockingQueue<String> receivedQueue = networking.getReceivedMessages();
		readMessages(receivedQueue);
	}
	
	private void sendMessages(ArrayBlockingQueue<String> sendQueue, PrintWriter out) {
		for (int i=0; i<sendQueue.size(); i++) {
			String message = sendQueue.poll();
			out.println(message);
		}
	}
	
	private void readMessages(ArrayBlockingQueue<String> receivedQueue) {
		for (int i=0; i<receivedQueue.size(); i++) {
			String message = receivedQueue.poll();
			String id = message.toUpperCase();
			if (message.contains(":")) {
				id = message.split(":")[0].toUpperCase();
			}
			int idIndex = message.indexOf(":");
			String payload = "";
			if (idIndex+1 < message.length()) {
				payload = message.substring(idIndex+1);
			}
			// CHAT:type,color,message,[playerid]
			if (id.equals("CHAT")) {
				logger.info("[chat] " + payload);
				String payloadParts[] = payload.split(",");
				if (payloadParts.length >= 3) {
					Entity chatEntity = world.createEntity();
					chatEntity.setGroup("CHAT");
					// SAY or BROADCAST
					String chatType = payloadParts[0];
					String color = payloadParts[1];
					chatEntity.addComponent(new ColorComponent(color));
					String chatMsg = "";
					
					if (chatType.equals("BROADCAST")) {
						chatMsg = payload.substring(payload.indexOf(color) + color.length() + 1);
						chatEntity.addComponent(new Timer(15 * 1000));
					} else if (chatType.equals("SAY")) {
						String playerId = payloadParts[2];
						chatMsg = payload.substring(payload.indexOf(playerId) + playerId.length() + 1);
						String bubbleMsg = chatMsg.substring(chatMsg.indexOf(":") + 1).trim();
						Entity player = world.getTagManager().getEntity(playerId);
						player.addComponent(new ChatBubble(bubbleMsg, 15 * 1000));
					}
					chatEntity.addComponent(new DrawableText(chatMsg));
					chatEntity.refresh();
				} else {
					logger.warn("Invalid CHAT:playerid,type,color,message");
				}
			// WARP:mapref,x,y
			} else if (id.equals("WARP")) {
				logger.info("[warp] " + payload);
				String payloadParts[] = payload.split(",");
				if (payloadParts.length == 3) {
					Warp warp = new Warp();
					warp.setMapRef(payloadParts[0]);
					int x = Integer.valueOf(payloadParts[1]);
					int y = Integer.valueOf(payloadParts[2]);
					warp.setPosition(new Vector2f(x,y));
					world.getTagManager().getEntity("YOU").addComponent(warp);
					world.getTagManager().getEntity("YOU").refresh();
				} else {
					logger.warn(payloadParts.length + "Invalid WARP:mapref,x,y");
				}
			// SET_REF:playerid,ref
			} else if (id.equals("SET_REF")) {
				logger.info("[set_ref] " + payload);
				String payloadParts[] = payload.split(",");
				Entity entity;
				entity = world.getTagManager().getEntity(payloadParts[0]);
				
				if (resourceRefMapper.get(entity) != null) {
					resourceRefMapper.get(entity).setResourceName(payloadParts[1]);
				} else {
					entity.addComponent(new ResourceRef(payloadParts[1]));
				}
			// PLAYER_JOINED_MAP:playerid,playerimage,x,y,playername
			} else if (id.equals("PLAYER_JOINED_MAP")) {
				logger.info("[player_joined_map] " + payload);
				String payloadParts[] = payload.split(",");
				if (payloadParts.length == 4) {
					Entity newPlayer = world.createEntity();
					newPlayer.setGroup("PLAYER");
					newPlayer.setTag(payloadParts[0]);
					newPlayer.addComponent(new ResourceRef(payloadParts[1]));
					newPlayer.addComponent(new Location(Integer.valueOf(payloadParts[2]), Integer.valueOf(payloadParts[3])));
					newPlayer.refresh();
				} else {
					logger.warn(payloadParts.length + "Invalid PLAYER_JOINED_MAP:playerid,playerimage,x,y");
				}
			// SET_NAME:playerid,name
			} else if (id.equals("SET_NAME")) {
				logger.info("[set_name] " + payload);
				String playerId = payload.substring(0, payload.indexOf(","));
				String playerName = payload.substring(payload.indexOf(",") + 1);
				Entity entity;
				entity = world.getTagManager().getEntity(playerId);
				
				if (drawableTextMapper.get(entity) != null) {
					drawableTextMapper.get(entity).setText(playerName);
				} else {
					entity.addComponent(new DrawableText(playerName));
				}
			// PLAYER_MOVED:playerid,x,y
			} else if (id.equals("PLAYER_MOVED")) {
				logger.info("[player_moved] " + payload);
				String payloadParts[] = payload.split(",");
				if (payloadParts.length == 3) {
					Entity playerEntity = world.getTagManager().getEntity(payloadParts[0]);
					Vector2f playerLocation = locationMapper.get(playerEntity).getPosition();
					playerLocation.set(new Vector2f(Integer.valueOf(payloadParts[1]), Integer.valueOf(payloadParts[2])));
				} else {
					logger.warn(payloadParts.length + "Invalid PLAYER_MOVED:playerid,x,y");
				}
			// PLAYER_LEFT_MAP:playerid
			} else if (id.equals("PLAYER_LEFT_MAP")) {
				logger.info("[player_left_map] " + payload);
				world.getTagManager().getEntity(payload).delete();
			// ENEMY_JOINED_MAP:enemyid,enemyimage,x,y,enemyname
			} else if (id.equals("ENEMY_JOINED_MAP")) {
				logger.info("[enemy_joined_map] " + payload);
				String payloadParts[] = payload.split(",");
				if (payloadParts.length == 4) {
					Entity newEnemy = world.createEntity();
					newEnemy.setGroup("ENEMY");
					newEnemy.setTag(payloadParts[0]);
					newEnemy.addComponent(new ResourceRef(payloadParts[1]));
					newEnemy.addComponent(new Location(Integer.valueOf(payloadParts[2]), Integer.valueOf(payloadParts[3])));
					newEnemy.refresh();
				} else {
					logger.warn(payloadParts.length + "Invalid ENEMY_JOINED_MAP,enemyid,enemyimage,x,y");
				}
			// ENEMY_LEFT_MAP:playerid
			} else if (id.equals("ENEMY_LEFT_MAP")) {
				logger.info("[enemy_left_map] " + payload);
				world.getTagManager().getEntity(payload).delete();
				// PLAYER_MOVED:playerid,x,y
			} else if (id.equals("ENEMY_MOVED")) {
				logger.info("[enemy_moved] " + payload);
				String payloadParts[] = payload.split(",");
				if (payloadParts.length == 3) {
					Entity enemyEntity = world.getTagManager().getEntity(payloadParts[0]);
					Vector2f enemyLocation = locationMapper.get(enemyEntity).getPosition();
					enemyLocation.set(new Vector2f(Integer.valueOf(payloadParts[1]), Integer.valueOf(payloadParts[2])));
				} else {
					logger.warn(payloadParts.length + "Invalid ENEMY_MOVED:playerid,x,y");
				}
			// ERROR:message
			} else if (id.equals("ALERT")) {
				logger.info("[alert]" + payload);
				game.enterState(LoginState.ID);
				Sys.alert("Alert", payload);
				
			} else {
				logger.warn("Not doing anything with the payload: " + payload);
			}
				
		}
	}
}
