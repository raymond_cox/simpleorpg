How to build
============

1. Download [Maven](http://maven.apache.org) and add it to your path environment variable

2. Git the source 

   `git clone https://github.com/coxry/SimpleOrpg.git`

3. Run Maven from the SimpleOrpg directory

   `cd SimpleOrpg`  
   `mvn clean package`

4. The executables are placed in `SimpleOrpg/SimpleOrpg-Client/target` and `SimpleOrpg/SimpleOrpg-Server/target`
