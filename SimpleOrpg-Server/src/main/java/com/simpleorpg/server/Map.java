package com.simpleorpg.server;

import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

import com.simpleorpg.common.Enemy;

/* The server's view of the map*/
public class Map {
	private final HashMap<Socket, Player> players = new HashMap<Socket, Player>();
	private ArrayList<Enemy> enemies = new ArrayList<Enemy>();
	
	public Map() {
	}

	public HashMap<Socket, Player> getPlayers() {
		return players;
	}
	
	public ArrayList<Enemy> getEnemies() {
		return enemies;
	}
	
	public void setEnemies(ArrayList<Enemy> enemies) {
		this.enemies = enemies;
	}

}
