package com.simpleorpg.server;

import org.apache.log4j.Logger;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.simpleorpg.common.Crypto;
import com.simpleorpg.common.Enemy;
import com.simpleorpg.common.PropertiesLoader;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Properties;

public class Database {
	private static final Database instance = new Database();
	private Logger logger = Logger.getLogger(Database.class);
	private static ComboPooledDataSource cpds;
	private final String selectUsers = "SELECT * FROM users WHERE username = ?";
	private final String insertUser = "INSERT INTO users VALUES (?, ?, ?, ?)";
	private final String selectPlayers = "SELECT * FROM players WHERE playerid IN (SELECT playerid FROM userplayers WHERE username = ?)";
	private final String selectEnemy = "SELECT * FROM enemies WHERE enemyid = ?";
	private final String insertPlayer = "INSERT INTO players VALUES (null, ?, ?, ?, ?, ?, ?)";
	private final String insertUserPlayer = "INSERT INTO userplayers VALUES (null, ?, ?)";
	private final String updatePlayer = "UPDATE players SET playername = ?, X = ?, Y = ?, mapref = ?, spriteref = ? WHERE playerid = ?";
	
	private Database() {
		Properties props = PropertiesLoader.getInstance().loadProperties("server.properties");
	    String url = props.getProperty("dbconnection");
		String driverClass = props.getProperty("driverclass");
		String minPoolSize = props.getProperty("minpoolsize", "5");
		String acquireIncrement = props.getProperty("acquireincrement", "5");
		String maxPoolSize = props.getProperty("maxpoolsize", "20");
		logger.info(url);
		logger.info(driverClass);
		cpds = new ComboPooledDataSource();
		
		try {
			cpds.setDriverClass(driverClass);
			cpds.setJdbcUrl(url);
			cpds.setMinPoolSize(Integer.valueOf(minPoolSize));
			cpds.setAcquireIncrement(Integer.valueOf(acquireIncrement));
			cpds.setMaxPoolSize(Integer.valueOf(maxPoolSize));
			
		} catch (PropertyVetoException e) {
			logger.fatal(e.getMessage(), e);
		}

	}

	public static Database getInstance() {
		return instance;
	}
	
	public Connection getConnection() throws SQLException {
		return cpds.getConnection();
	}
	
	public boolean correctPassword(String user, String pass) {
		boolean valid = false;
		
		try {
			Connection conn = getConnection();
			PreparedStatement selectUsersPS = conn.prepareStatement(selectUsers);
			selectUsersPS.setString(1, user);
			ResultSet rs = selectUsersPS.executeQuery();
			
			if (rs.next()) {
				String salt = rs.getString("salt");
				Crypto crypto = Crypto.getInstance();
				String[] hashSalt = crypto.getHashSalt(pass, salt);

				if ((rs.getString("password")).equals(hashSalt[0])) {
					valid = true;
				}
			}
			conn.close();
		} catch (SQLException ex) {
			logger.error("validPassword", ex);
		}
		return valid;
	}
	
	public boolean insertUser(String user, String pass, String salt) {
		boolean valid = false;
		try {
			Connection conn = getConnection();
			PreparedStatement insertUserPS = conn.prepareStatement(insertUser);
			Timestamp created = new Timestamp(new java.util.Date().getTime());
			insertUserPS.setString(1, user);
			insertUserPS.setString(2, pass);
			insertUserPS.setString(3, salt);
			insertUserPS.setTimestamp(4, created);
			insertUserPS.execute();
			valid = true;
			conn.close();
		} catch (SQLException ex) {
			logger.error("insertUser", ex);
		}
		return valid;
	}

	public int insertPlayer(Player yourPlayer) {
		int pid = -1;
		try {
			Connection conn = getConnection();
			PreparedStatement insertPlayerPS = conn.prepareStatement(insertPlayer, Statement.RETURN_GENERATED_KEYS);
			PreparedStatement insertUserPlayerPS = conn.prepareStatement(insertUserPlayer);
			insertPlayerPS.setString(1, yourPlayer.getName());
			insertPlayerPS.setInt(2, yourPlayer.getX());
			insertPlayerPS.setInt(3, yourPlayer.getY());
			insertPlayerPS.setString(4, yourPlayer.getMapRef());
			insertPlayerPS.setString(5, yourPlayer.getRef());
			insertPlayerPS.setInt(6, 0);
			insertPlayerPS.executeUpdate();
			ResultSet rs = insertPlayerPS.getGeneratedKeys();
			if (rs.next()){
				pid=rs.getInt(1);
			}
			insertUserPlayerPS.setString(1, yourPlayer.getUser());
			insertUserPlayerPS.setInt(2, pid);
			insertUserPlayerPS.execute();
			conn.close();
		} catch (SQLException ex) {
			logger.error("insertPlayer", ex);
		}
		return pid;
	}

	public ArrayList<Player> selectPlayers(String user) {
		ArrayList<Player> players = new ArrayList<Player>();
		try {
			Connection conn = getConnection();
			PreparedStatement selectPlayersPS = conn.prepareStatement(selectPlayers);
			selectPlayersPS.setString(1, user);
			ResultSet rs = selectPlayersPS.executeQuery();
			
			while (rs.next()) {
				int id = rs.getInt("playerid");
				String name = rs.getString("playername");
				int x = rs.getInt("x");
				int y = rs.getInt("y");
				String mapRef = rs.getString("mapref");
				String ref = rs.getString("spriteref");
				int admin = rs.getInt("admin");
				Player player = new Player(id, name, ref, admin, mapRef);
				player.setLocation(x, y);
				players.add(player);
			}
			conn.close();
		} catch (SQLException ex) {
			logger.error("selectPlayers", ex);
		}
		return players;
	}
	
	public Enemy selectEnemy(String enemyId) {
		Enemy enemy = new Enemy();
		try {
			Connection conn = getConnection();
			PreparedStatement selectEnemyPS = conn.prepareStatement(selectEnemy);
			selectEnemyPS.setString(1, enemyId);
			ResultSet rs = selectEnemyPS.executeQuery();
			rs.next();
			String ref = rs.getString("spriteref");
			enemy.setRef(ref);
			conn.close();
		} catch (SQLException ex) {
			logger.error("selectEnemy", ex);
		}
		return enemy;
	}

	public boolean updatePlayer(Player yourPlayer) {
		boolean valid = false;
		try {
			Connection conn = getConnection();
			PreparedStatement updatePlayerPS = conn.prepareStatement(updatePlayer);
			updatePlayerPS.setString(1, yourPlayer.getName());
			updatePlayerPS.setInt(2, yourPlayer.getX());
			updatePlayerPS.setInt(3, yourPlayer.getY());
			updatePlayerPS.setString(4, yourPlayer.getMapRef());
			updatePlayerPS.setString(5, yourPlayer.getRef());
			updatePlayerPS.setInt(6, yourPlayer.getId());
			updatePlayerPS.execute();
			valid = true;
			conn.close();
		} catch (SQLException ex) {
			logger.error("updatePlayer", ex);
		}
		return valid;
	}
	
}
