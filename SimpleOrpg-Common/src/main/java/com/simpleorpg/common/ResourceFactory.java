package com.simpleorpg.common;

import java.beans.XMLDecoder;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.simpleorpg.common.Resource.ResourceType;

public class ResourceFactory {
	private final Logger logger = Logger.getLogger(getClass());
	private static final ResourceFactory instance = new ResourceFactory();
	private HashMap<String, Element> resourceElements = new HashMap<String, Element>();

	private ResourceFactory() {
	}

	public static ResourceFactory getInstance() {
		return instance;
	}

	public Resource create(String id, boolean headless) throws SlickException {
		id = id.toLowerCase();
		Element resourceElement = resourceElements.get(id);
		if (resourceElement == null)
			throw new RuntimeException("Cannot find ID " + id
					+ " in XML document");
		String type = resourceElement.getAttribute("type").toLowerCase();
		String path = "resources/" + resourceElement.getAttribute("path");
		try {
			path = new File(path).getCanonicalPath();
		} catch (IOException e) {
			logger.error(e);
		}

		if (type.equals("image")) {
			return new Resource(id, ResourceType.IMAGE, path, new Image(path));
		} else if (type.equals("tiledmap")) {
			if (headless) {
				return new Resource(id, ResourceType.TILED_MAP, path,
						new NewTiledMap(path, false));
			} else {
				return new Resource(id, ResourceType.TILED_MAP, path,
						new NewTiledMap(path, true));
			}
		} else if (type.equals("spritesheet")) {
			int width = Integer.valueOf(resourceElement.getAttribute("width"));
			int height = Integer
					.valueOf(resourceElement.getAttribute("height"));
			return new Resource(id, ResourceType.SPRITE_SHEET, path,
					new SpriteSheet(path, width, height));
		} else if (type.equals("enemy")) {
			Enemy enemy = (Enemy)deserializeXMLToObject(path);
			return new Resource(id, ResourceType.ENEMY, path, enemy);
		}

		return null;
	}

	private Object deserializeXMLToObject(String xmlFileLocation) {
		Object deSerializedObject = null;
		try {
			FileInputStream os = new FileInputStream(xmlFileLocation);
			XMLDecoder decoder = new XMLDecoder(os);
			deSerializedObject = decoder.readObject();
			decoder.close();
		} catch (Exception ex) {
			logger.error(ex);
		}
		return deSerializedObject;
	}

	public HashMap<String, Element> loadResources(InputStream is)
			throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder docBuilder = null;
		docBuilder = docBuilderFactory.newDocumentBuilder();
		Document doc = null;
		doc = docBuilder.parse(is);

		// normalize text representation
		doc.getDocumentElement().normalize();

		NodeList listResources = doc.getElementsByTagName("resource");

		int totalResources = listResources.getLength();

		for (int resourceIdx = 0; resourceIdx < totalResources; resourceIdx++) {
			Node resourceNode = listResources.item(resourceIdx);
			if (resourceNode.getNodeType() == Node.ELEMENT_NODE) {
				Element resourceElement = (Element) resourceNode;
				String id = resourceElement.getAttribute("id");
				resourceElements.put(id.toLowerCase(), resourceElement);
			}
		}

		return resourceElements;

	}

	public ArrayList<String> getResourceIds(String resId) {
		ArrayList<String> resourceIds = new ArrayList<String>();

		for (Element resourceElement : resourceElements.values()) {
			String type = resourceElement.getAttribute("type").toLowerCase();

			if (type.equals(resId)) {
				String id = resourceElement.getAttribute("id").toLowerCase();
				resourceIds.add(id);
			}
		}

		return resourceIds;
	}

}
