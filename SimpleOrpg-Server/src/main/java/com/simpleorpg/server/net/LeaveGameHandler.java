package com.simpleorpg.server.net;

import java.net.Socket;

import org.apache.log4j.Level;

import com.simpleorpg.server.Player;

public class LeaveGameHandler extends MessageHandler {

	@Override
	public void handleMessage(Socket socket) {
		try {
			synchronized(this) {
				log(Level.DEBUG, MSG_TYPE.REC, socket, "LEAVE_GAME");
				Player yourPlayer = players.get(socket);
				if (players.containsKey(socket)) {
					db.updatePlayer(yourPlayer);
					String leaveMessage = "CHAT:BROADCAST,#FF0000," + yourPlayer.getName() + " has left the game!";
					sendAll(leaveMessage);
					MessageHandler leaveMapHandler = new LeaveMapHandler();
					leaveMapHandler.handleMessage(socket);
					// Remove you from the game
					players.remove(socket);
				}
				if (users.containsKey(socket)) users.remove(socket);
				
				closeSocket(socket);
			}

		} catch (Exception ex) {
			log(Level.ERROR, socket, ex.getMessage(), ex.getCause());
		}
	}

}
