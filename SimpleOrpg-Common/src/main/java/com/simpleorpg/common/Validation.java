package com.simpleorpg.common;

public class Validation {
	private static final Validation instance = new Validation();
	
	public static Validation getInstance() {
		return instance;
	}
	
	public boolean validUsername(String userName) {
		boolean valid = false;
		if (isCharacter(userName)) {
			if (userName.length() >= 3 && userName.length() <= 10) {
				valid = true;
			}
		}
		return valid;
	}
	
	public boolean validPassword(String password) {
		boolean valid = false;
		if (password.length() >= 3 && password.length() <= 20) {
			valid = true;
		}
		return valid;
	}
	
	public boolean validPlayer(String playerName) {
		boolean valid = false;
		if (isCharacter(playerName)) {
			if (playerName.length() >= 3 && playerName.length() <= 20) {
				valid = true;
			}
		}
		return valid;
	}
	
	public boolean isCharacter(String st) {
		for (char c : st.toCharArray()) {
			if (c <= 64 || (c >= 91 && c <= 96) || c >= 123) {
				return false;
			}
		}
		return true;
	}
	
	public boolean isNumeric(String st) {
		for (char c : st.toCharArray()) {
			if (c <= 47 || c >= 58) {
				return false;
			}
		}
		return true;
	}
	
	public boolean isAlpha(String st) {
		for (char c : st.toCharArray()) {
			if (c <= 31 || c >= 127) {
				return false;
			}
		}
		
		return true;
	}
}
