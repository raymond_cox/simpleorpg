package com.simpleorpg.server.net;
import java.net.Socket;

import org.apache.log4j.Level;
public class AlertHandler extends MessageHandler {
	private String payload;
	
	public AlertHandler(String payload) {
		this.payload = payload;
	}

	@Override
	public void handleMessage(Socket socket) {
		log(Level.DEBUG, MSG_TYPE.REC, socket, "ALERT " + payload);
		sendTo(socket, "ALERT:"+payload);
	}

}
