package com.simpleorpg.client.states;

import org.apache.log4j.Logger;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import com.artemis.World;

public abstract class BaseGameState extends BasicGameState {
	protected final Logger logger = Logger.getLogger(getClass());
	protected World world;

	public BaseGameState(World world) {
		this.world = world;
	}
	
	public void init(GameContainer container, StateBasedGame game)
			throws SlickException {
		logger.debug("Init");
		container.setAlwaysRender(true);
		container.setVSync(true);
		container.setShowFPS(false);
	}
	
	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {
		world.loopStart();
		world.setDelta(delta);
	}
	
	@Override
	public void enter(GameContainer container, StateBasedGame game)
			throws SlickException {
		super.enter(container, game);
		logger.debug("Entered State");
	}
}
