package com.simpleorpg.client;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.newdawn.slick.Font;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.gui.GUIContext;
import org.newdawn.slick.gui.TextField;

public class TabbedTextField extends TextField {
	protected final Logger logger = Logger.getLogger(getClass());
	
	private final int tab;
	private static final ArrayList<TabbedTextField> fields = new ArrayList<TabbedTextField>();
	private static int selectedTab = 0;
	
	public TabbedTextField(GUIContext container, Font font, int x, int y,
			int width, int height, int order) {
		super(container, font, x, y, width, height);
		this.tab = order;
		fields.add(this);
	}
	
	@Override
	public void render(GUIContext container, Graphics g) {
		super.render(container, g);
	}
	
	@Override
	public void keyPressed(int key, char c) {
		super.keyPressed(key, c);
		if (key == 15 && this.hasFocus()) {
			this.setFocus(false);
			if (++selectedTab >= fields.size()) selectedTab = 0;
			for (TabbedTextField field : fields) {
				if (field.getTab() == selectedTab) {
					field.setFocus(true);
					container.getInput().consumeEvent();
					break;
				}
			}
		}
	}
	
	@Override
	public void setFocus(boolean focus) {
		super.setFocus(focus);
		
		if (focus) {
			selectedTab = tab;
		}
	}

	public int getTab() {
		return tab;
	}
	
	
	
}
