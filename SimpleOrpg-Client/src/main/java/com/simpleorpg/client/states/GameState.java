package com.simpleorpg.client.states;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import com.artemis.EntitySystem;
import com.artemis.SystemManager;
import com.artemis.World;
import com.simpleorpg.client.systems.InputSystem;
import com.simpleorpg.client.systems.RenderSystem;
import com.simpleorpg.client.systems.WarpSystem;

public class GameState extends BaseGameState {
	public static final int ID = 1;
	private SystemManager systemManager;
	private EntitySystem renderSystem;
	private EntitySystem warpSystem;
	private EntitySystem inputSystem;
	
	public GameState(World world) {
		super(world);

	}
	
	@Override
	public void init(GameContainer container, StateBasedGame game)
			throws SlickException {
		systemManager = world.getSystemManager();
		renderSystem = systemManager.setSystem(new RenderSystem(container));
		warpSystem = systemManager.setSystem(new WarpSystem());
		inputSystem = systemManager.setSystem(new InputSystem(container, game));
		systemManager.initializeAll();
		super.init(container, game);
	}

	public void render(GameContainer container, StateBasedGame game, Graphics g)
			throws SlickException {
		renderSystem.process();
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {
		inputSystem.process();
		warpSystem.process();
	}

	@Override
	public int getID() {
		return ID;
	}

}
