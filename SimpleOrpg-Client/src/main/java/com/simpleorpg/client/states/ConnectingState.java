package com.simpleorpg.client.states;

import java.util.Properties;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import com.artemis.World;
import com.simpleorpg.common.PropertiesLoader;

public class ConnectingState extends BaseGameState {
	public static final int ID = 2;
	
	public ConnectingState(World world) {
		super(world);
	}

	@Override
	public void init(GameContainer container, StateBasedGame game)
			throws SlickException {
		super.init(container, game);
	}

	public void render(GameContainer container, StateBasedGame game, Graphics g)
			throws SlickException {
		Properties props = PropertiesLoader.getInstance().loadProperties("client.properties");
		String ip = props.getProperty("game_ip", "127.0.0.1");
		int port = Integer.valueOf(props.getProperty("game_port", "1234"));
		String connectingString = "Connecting to " + ip + ":" + port + "...";
		int cw = g.getFont().getWidth(connectingString);
		int ch = g.getFont().getHeight(connectingString);
		g.getFont().drawString(container.getWidth()/2 - cw/2, container.getHeight()/2 - ch/2, connectingString, Color.white);
	}

	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {
		
	}

	@Override
	public int getID() {
		return ID;
	}

}
