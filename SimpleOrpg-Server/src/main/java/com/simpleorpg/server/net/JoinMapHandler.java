package com.simpleorpg.server.net;

import java.net.Socket;

import org.apache.log4j.Level;

import com.simpleorpg.common.Enemy;
import com.simpleorpg.server.Map;
import com.simpleorpg.server.Player;

public class JoinMapHandler extends MessageHandler {
	
	public JoinMapHandler() {
	}

	@Override
	public void handleMessage(Socket socket) {
		try {
			synchronized(this) {
				log(Level.DEBUG, MSG_TYPE.REC, socket, "JOIN_MAP");
				Player yourPlayer = players.get(socket);
				Map map = maps.get(yourPlayer.getMapRef());
				
				// Send all other players on the map your player
				String youJoinedMap = "PLAYER_JOINED_MAP:" + yourPlayer.getId() + "," + 
															 yourPlayer.getRef() + "," + 
															 yourPlayer.getX() + "," + 
															 yourPlayer.getY() + "\n";
				youJoinedMap += "SET_NAME:" + yourPlayer.getId() + "," + yourPlayer.getName();
				
				String otherJoinedMap = "";
				for (Socket otherSocket : map.getPlayers().keySet()) {
					Player player = map.getPlayers().get(otherSocket);
					// Send you to all other players on the map
					if (!otherJoinedMap.equals("")) otherJoinedMap += "\n";
					otherJoinedMap += "PLAYER_JOINED_MAP:" + player.getId() + "," + 
														     player.getRef() + "," + 
														     player.getX() + "," + 
														     player.getY() + "\n";
					otherJoinedMap += "SET_NAME:" + player.getId() + "," + player.getName();
					
					sendTo(otherSocket, youJoinedMap);
				}
				
				String enemyJoinedMap = "";
				for (Enemy enemy : map.getEnemies()) {
					// Send you to all other enemies on the map
					if (!enemyJoinedMap.equals("")) enemyJoinedMap += "\n";
					enemyJoinedMap += "ENEMY_JOINED_MAP:" + enemy.getId() + "," + 
														    enemy.getRef() + "," + 
														    enemy.getX() + "," + 
														    enemy.getY();					
				}

				// Add you to the map
				map.getPlayers().put(socket, yourPlayer);
				
				// Send you all other players and enemies on the map
				sendTo(socket, otherJoinedMap);
				sendTo(socket, enemyJoinedMap);
				
				// Send you the warp message
				String warpMessage = "WARP:" + yourPlayer.getMapRef() + "," + yourPlayer.getX() + "," + yourPlayer.getY();
				sendTo(socket, warpMessage);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(ex);
		}
	}

}
