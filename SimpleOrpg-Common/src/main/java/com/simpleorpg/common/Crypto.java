package com.simpleorpg.common;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.*;
import java.util.Random;

import org.apache.log4j.Logger;

public final class Crypto {

	private static final Crypto instance = new Crypto();
	private Logger logger = Logger.getLogger(this.getClass());
	private Random random;

	private Crypto() {
		random = new Random(System.currentTimeMillis());
	}

	public static Crypto getInstance() {
		return instance;
	}

	private String createHash(String message) {
		String hexString = message;
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			byte[] dataBytes = new byte[1024];

			int nread = 0;
			ByteArrayInputStream bis = new ByteArrayInputStream(
					message.getBytes("UTF-8"));

			while ((nread = bis.read(dataBytes)) != -1) {
				md.update(dataBytes, 0, nread);
			}
			byte[] mdbytes = md.digest();

			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < mdbytes.length; i++) {
				sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16)
						.substring(1));
			}
			hexString = sb.toString();
		} catch (IOException ex) {
			logger.error("getHash", ex);
		} catch (NoSuchAlgorithmException ex) {
			logger.error("getHash", ex);
		}
		return hexString;

	}
	
	public String[] getHashSalt(String message, String salt) {
		String hash = message;
		
		hash = createHash(hash);
		hash += salt;
		
		for (int i=0; i<1000; i++) {
			hash = createHash(hash);
		}
		
		String[] hashSalt = new String[2];
		hashSalt[0] = hash;
		hashSalt[1] = salt;
	
		return hashSalt;
	}

	public String[] getHashSalt(String message) {
		String salt = generateString("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890", 16);
		return getHashSalt(message, salt);
	}

	private String generateString(String characters, int length) {
		char[] text = new char[length];
		for (int i = 0; i < length; i++) {
			text[i] = characters.charAt(random.nextInt(characters.length()));
		}
		return new String(text);
	}
}
