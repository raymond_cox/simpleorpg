package com.simpleorpg.client.states;

import java.awt.Font;
import java.util.Properties;

import org.lwjgl.Sys;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.gui.AbstractComponent;
import org.newdawn.slick.gui.ComponentListener;
import org.newdawn.slick.gui.MouseOverArea;
import org.newdawn.slick.state.StateBasedGame;

import com.artemis.Entity;
import com.artemis.World;
import com.simpleorpg.client.PasswordTextField;
import com.simpleorpg.client.TabbedTextField;
import com.simpleorpg.client.components.Movement;
import com.simpleorpg.client.components.Networking;
import com.simpleorpg.common.PropertiesLoader;
import com.simpleorpg.common.ResourceManager;
import com.simpleorpg.common.Validation;
@SuppressWarnings("deprecation")
public class LoginState extends BaseGameState {
	public static final int ID = 0;
	private MouseOverArea loginButton;
	private MouseOverArea newButton;
	private TabbedTextField userField;
	private PasswordTextField passField;
	private TrueTypeFont inputFont;
	private TrueTypeFont logoFont;
	private int cx, cy;
	private String userText = "User:";
	private int userTextX, userTextY;
	private String passText = "Pass:";
	private int passTextX, passTextY;
	private String logo = "Simple ORPG";
	private int logoX, logoY;
	
	public LoginState(World world) {
		super(world);
	}

	@Override
	public void enter(GameContainer container, StateBasedGame game)
			throws SlickException {
		super.enter(container, game);
		userField.setText("");
		passField.setText("");
	}
	
	@Override
	public void init(GameContainer container, final StateBasedGame game)
			throws SlickException {
		super.init(container, game);
		
		// Setup networking
		Entity player = world.getTagManager().getEntity("YOU");
		Properties props = PropertiesLoader.getInstance().loadProperties("client.properties");
		String ip = props.getProperty("game_ip", "127.0.0.1");
		int port = Integer.valueOf(props.getProperty("game_port", "1234"));
		player.addComponent(new Networking(ip, port));
		player.addComponent(new Movement(100));
		player.refresh();
		
		ResourceManager manager = ResourceManager.getInstance();
		Image loginImage = (Image)manager.getResource("loginImage").getObject();
		Image newImage = (Image)manager.getResource("newImage").getObject();
		cx = container.getWidth()/2;
		cy = container.getHeight()/2;
		inputFont = new TrueTypeFont(new java.awt.Font("Verdana", Font.BOLD, 12), false);
		logoFont = new TrueTypeFont(new java.awt.Font("Verdana", Font.PLAIN, 40), false);
		
		logoX = cx - logoFont.getWidth(logo)/2;
		logoY = cy - logoFont.getHeight()/2 - 130;
		
		userTextX = logoX + logoFont.getWidth(logo)/2 - (inputFont.getWidth(userText) + 20)/2 - 125/2;
		userTextY = logoY + logoFont.getHeight() + 80;
		userField = new TabbedTextField(container, inputFont, userTextX + inputFont.getWidth(userText) + 20, userTextY, 125, 20, 0);
		userField.setBackgroundColor(new Color(70,72,62,255));
		userField.setTextColor(new Color(255, 255, 255));
		userField.setFocus(true);
		userField.setBorderColor(new Color(70-40,72-40,62-40,255));
		
		passTextX = userTextX;
		passTextY = userTextY + inputFont.getHeight() + 20;
		passField = new PasswordTextField(container, inputFont, passTextX + inputFont.getWidth(passText) + 20, passTextY, 125, 20, 1);
		passField.setBackgroundColor(new Color(70,72,62,255));
		passField.setTextColor(new Color(255, 255, 255));
		passField.setBorderColor(new Color(70-40,72-40,62-40,255));
		
		loginButton = new MouseOverArea(container, loginImage, passField.getX() + passField.getWidth() - loginImage.getWidth(), passField.getY() + passField.getHeight() + 20);
		newButton = new MouseOverArea(container, newImage, passField.getX() + 15, passField.getY() + passField.getHeight() + 20);

		loginButton.addListener(new ComponentListener() {
			public void componentActivated(AbstractComponent source) {
				String userName = userField.getText();
				String password = passField.getText();
				Validation validation = Validation.getInstance();
				Networking networking = world.getTagManager().getEntity("YOU").getComponent(Networking.class);
				
				if (validation.validUsername(userName) && validation.validPassword(password)) {
					networking.getSendMessages().add("LOGIN:" + userField.getText() + "," + passField.getText());
					game.enterState(ConnectingState.ID);
				} else {
					Sys.alert("Error", "Invalid username/password!");
					userField.setText("");
					passField.setText("");
				}
			}
		});
		
		newButton.addListener(new ComponentListener() {
			public void componentActivated(AbstractComponent source) {
				String userName = userField.getText();
				String password = passField.getText();
				Validation validation = Validation.getInstance();
				Networking networking = world.getTagManager().getEntity("YOU").getComponent(Networking.class);

				if (validation.validUsername(userName) && validation.validPassword(password)) {
					networking.getSendMessages().add("NEW:" + userField.getText() + "," + passField.getText());
					game.enterState(ConnectingState.ID);
				} else {
					Sys.alert("Error", "Invalid username/password!");
					userField.setText("");
					passField.setText("");
				}
			}
		});
	}
	

	public void render(GameContainer container, StateBasedGame game, Graphics g)
			throws SlickException {
		g.setColor(new Color(51, 52, 45,255));
		//g.setColor(new Color(70,72,62,255));
		
		g.fillRect(0, 0, container.getWidth(), container.getHeight());
		
		logoFont.drawString(logoX, logoY, logo, new Color(210,216,186));
		g.setColor(new Color(255,255,255,255));
		inputFont.drawString(userTextX, userTextY, userText, new Color(210,216,186));
		inputFont.drawString(passTextX, passTextY, passText, new Color(210,216,186));

		userField.render(container, g);
		passField.render(container, g);
		loginButton.render(container, g);
		newButton.render(container, g);
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {
		super.update(container, game, delta);
	}

	@Override
	public int getID() {
		return ID;
	}

}
