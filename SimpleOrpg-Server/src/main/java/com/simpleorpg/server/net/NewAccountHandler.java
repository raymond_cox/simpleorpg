package com.simpleorpg.server.net;

import java.net.Socket;

import org.apache.log4j.Level;

import com.simpleorpg.common.Crypto;
import com.simpleorpg.common.PropertiesLoader;
import com.simpleorpg.server.Player;
public class NewAccountHandler extends MessageHandler {
	
	private String payload;
	public NewAccountHandler(String payload) {
		this.payload = payload;
	}

	@Override
	public void handleMessage(Socket socket) {
		synchronized(this) {
			log(Level.DEBUG, MSG_TYPE.REC, socket, "NEW:"+payload);
			
			if (payload.contains(",")) {
				String userName = payload.split(",")[0].toLowerCase();
				String password = payload.substring(userName.length() + 1);
				Crypto crypto = Crypto.getInstance();
	
				if (validation.validUsername(userName) && validation.validPassword(password)) {
					String[] hashSalt = crypto.getHashSalt(password);
					String passHash = hashSalt[0];
					String passSalt = hashSalt[1];
					if (db.insertUser(userName, passHash, passSalt)) {
						Player yourPlayer = createNewPlayer(userName, userName);
						int id = db.insertPlayer(yourPlayer);
						if (id != -1) {
							yourPlayer.setId(id);
						} else {
							throw new RuntimeException("Unable to insert " + yourPlayer.getName() + " into the database");
						}
						
						MessageHandler successHandler = new AlertHandler("Your account has been successfully created!");
						successHandler.handleMessage(socket);
						closeSocket(socket);
					} else {
						MessageHandler errorHandler = new AlertHandler("User already exists!");
						errorHandler.handleMessage(socket);
						closeSocket(socket);
					}
					
				} else {
					closeSocket(socket);
				}
			}
		}
	}
	
	private Player createNewPlayer(String playerName, String userName) {
		String startLocation = PropertiesLoader.getInstance().loadProperties("server.properties").getProperty("startlocation", "");
		String startMap = startLocation;
		int startX = 5, startY = 5;
		
		if (startLocation.contains(",")) {
			startMap = startLocation.split(",")[0].trim();
			startX = Integer.valueOf(startLocation.split(",")[1].trim());
			startY = Integer.valueOf(startLocation.split(",")[2].trim());
		}
		Player yourPlayer = new Player(playerName, 
								   	   "knightImage",
								   	   0,
								   	   startMap);
		if ((int)(Math.random()*2) == 0) yourPlayer.setRef("mageImage");
		yourPlayer.setLocation(startX, startY);
		yourPlayer.setUser(userName);
		return yourPlayer;
	}

}
