package com.simpleorpg.server;

public class Player {
	private String name;
	private String user;
	private String ref;
	private String mapRef;
	private Long lastMovedTime=0l;
	private int x=0, y=0;
	private int id;
	private int admin;
	
	public Player(int id, String name, String ref, int admin, String mapRef) {
		setName(name);
		setRef(ref);
		setMapRef(mapRef);
		setId(id);
		setAdmin(admin);
	}
	
	public Player(String name, String ref, int admin, String mapRef) {
		setName(name);
		setRef(ref);
		setMapRef(mapRef);
		setAdmin(admin);
	}
	
	public void setLocation(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref.toLowerCase();
	}
	
	@Override
	public String toString() {
		return (id + "," + mapRef + "," + name + "," + ref + "," + x + "," + y);
	}

	public String getMapRef() {
		return mapRef;
	}

	public void setMapRef(String mapRef) {
		this.mapRef = mapRef.toLowerCase();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Long getLastMovedTime() {
		return lastMovedTime;
	}

	public void setLastMovedTime(Long lastMovedTime) {
		this.lastMovedTime = lastMovedTime;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public int getAdmin() {
		return admin;
	}

	public void setAdmin(int admin) {
		this.admin = admin;
	}
	

}
